# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_userpushtoken_device'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='fb_id',
        ),
        migrations.RemoveField(
            model_name='user',
            name='fb_token',
        ),
        migrations.AddField(
            model_name='user',
            name='biz_id',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='is_fleeter',
            field=models.BooleanField(default=False),
        ),
    ]
