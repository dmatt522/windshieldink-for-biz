# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_user_active_plates'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='used_plates',
            field=models.IntegerField(default=0, blank=True),
        ),
    ]
