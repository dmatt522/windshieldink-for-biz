# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0009_remove_user_biz_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='User',
            name='username',
            field=models.CharField(error_messages={'unique': 'A user with that username already exists.'}, max_length=255, verbose_name='username', validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters.', 'invalid')], help_text='Required. 255 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True),
        ),
    ]
