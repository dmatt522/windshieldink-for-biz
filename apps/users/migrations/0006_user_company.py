# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0007_remove_company_owner'),
        ('users', '0005_auto_20160804_1721'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='company',
            field=models.ForeignKey(related_name='fleeter_company', blank=True, to='business.Company', null=True),
        ),
    ]
