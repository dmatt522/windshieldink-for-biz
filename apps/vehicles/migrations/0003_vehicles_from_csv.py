# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0002_auto_20160803_1551'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehicles',
            name='from_csv',
            field=models.BooleanField(default=False),
        ),
    ]
