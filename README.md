# Windshieldink For BIZ
######The Business Service of Windshieldink (“WSI”) provides two separate service features that will require payment; Fleet License Plates and Business Messaging.
***

## Requirements

- Python (v2.7, v3.4, v3.5)
- Django (v1.8.4)
- Django Rest Framework (v3.1.1)
- Django Stripe Payments (v2.0)
- Jinja2 (v2.8)
- Payments (v0.2)
- Pillow (v2.8.1)
- Gunicorn (v19.3.0)
- Twilio (v4.4.0)
