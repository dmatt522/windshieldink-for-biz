server {
	listen 80;
	server_name fleeter.windshieldink.loc;
	access_log /home/ubuntu/projects/windshieldink/windshieldink-for-biz/logs/access.log;
    error_log /home/ubuntu/projects/windshieldink/windshieldink-for-biz/logs/error.log;


	location = /favicon.ico { access_log off; log_not_found off; }
        location /static/ {
		root /home/ubuntu/projects/windshieldink/windshieldink-for-biz;
	}

	location / {
		include 	uwsgi_params;
		uwsgi_pass 	unix:/home/ubuntu/projects/windshieldink/windshieldink-for-biz/wsi/fleeter.sock;
	}
}
