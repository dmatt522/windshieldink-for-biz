from django.contrib.auth import authenticate, login, logout
from django.views.generic import TemplateView, View, FormView, RedirectView
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

from fleeter.account.forms import CustomUserCreationForm, CustomUserLoginForm
from apps.users.models import User as Account
from fleeter.account.forms import ImageUploadForm

from fleeter.business.models import Company

from wsi.fleeter_settings import SERVER_HOST_NAME
from apps.users.utils import generate_rand_number, send_mail_template
from wsi.utils.auth import generate_challange

import logging


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)


class UserCreateView(FormView):
    form_class = CustomUserCreationForm
    template_name = 'users/register.html'

    def get_biz_id(self):
        biz_id = generate_rand_number(12)
        tmp = Company.objects.filter(biz_id=biz_id).first()
        while (tmp is not None):
            biz_id = generate_rand_number(12)
            tmp = Company.objects.filter(biz_id=biz_id).first()

        return biz_id

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        context = self.get_context_data(**kwargs)
        context['form'] = form
        if form.is_valid():
            account = form.save(commit=False)
            account.is_active = False
            account.username = '{email}_fleeter'.format(
                email=account.email
            )
            account.is_fleeter = True
            account.challenge = generate_challange()

            company = Company(biz_id=self.get_biz_id())
            company.save()
            account.company = company
            account.save()

            try:
                send_mail_template(
                    'admin@windshieldink.com',
                    account.email,
                    "Activate Your Windshieldink Account",
                    "email_template/user/register",
                    {
                        'server_addr': SERVER_HOST_NAME,
                        'challenge': account.challenge,
                        'display_name': account.display_name
                    })
            except:
                logging.error("Register User - Error to send email to {0}".format(account.email))
                pass
            # write_log(user, 'Registered user - {email}'.format(email=user.email))
            return self.render_to_response({
                'message': 'Your Account has been created. An email has been sent to you for verification purposes. Please click on that email link to verify your account',
                'msg_class': 'success'
            })
        else:
            messages.error(self.request, 'Invalid form data!')
            return self.render_to_response({
                'message': 'Invalid form data!',
                'msg_class': 'danger'
            })

    def get(self, request, *args, **kwargs):
        form = self.form_class
        return self.render_to_response({'form': form})


class LoginView(FormView):
    form_class = CustomUserLoginForm
    template_name = 'users/login.html'
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context['page'] = 'login'
        return context

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        context = self.get_context_data(**kwargs)
        context['form'] = form

        if form.is_valid():
            account = authenticate(
                username='{email}_fleeter'.format(
                    email=form.cleaned_data['username']
                ),
                password=form.cleaned_data['password']
            )

            if account is not None and account.is_fleeter is True:
                if account.is_active:
                    login(request, account)

                    # write_log(account, 'Logged in from {email}'.format(email=account.email))

                    return HttpResponseRedirect(self.success_url)
                else:
                    context['error'] = 'Not activated account!'
                    return self.render_to_response(context)
            else:
                context['error'] = 'Invalid email or password!'
                return self.render_to_response(context)
        else:
            context['error'] = 'Invalid form data!'
            return self.render_to_response(context)


class LogoutView(LoginRequiredMixin, RedirectView):
    url = '/login/'

    def get(self, request, *args, **kwargs):
        # write_log(request.user, '{email} logged out'.format(email=request.user.email))
        logout(request)

        return super(LogoutView, self).get(request, *args, **kwargs)


class UploadAvatar(FormView):
    form_class = ImageUploadForm

    def post(self, request, *args, **kwargs):
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            m = Account.objects.get(pk=request.user.pk)
            m.avatar = form.cleaned_data['image']
            m.save()

        return redirect('/account/profile/')


class ProfileView(TemplateView):
    template_name = 'users/profile.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        context['page'] = 'settings'
        return context

    def get(self, request, *args, **kwargs):

        return self.render_to_response(context={
            'page': 'settings',
            'message': '',
        })

    def post(self, request, *args, **kwargs):
        user = request.user

        email = request.POST.get('email', '')
        exist = Account.objects.filter(email=email).first()
        if email != user.email:
            if exist is not None:
                if user is not exist:
                    message = 'Failed to update!'
                    message_class = 'alert alert-danger'
                    message_icon = 'fa fa-close'

                    return self.render_to_response(context={
                        'page': 'settings',
                        'message': message,
                        'message_class': message_class,
                        'message_icon': message_icon,
                    })

        current_pwd = request.POST.get('current_pwd', '')
        new_pwd = request.POST.get('new_pwd', '')
        conf_pwd = request.POST.get('conf_pwd', '')

        if current_pwd != '' and new_pwd != '' and new_pwd == conf_pwd:
            test_auth = authenticate(
                username='{email}_fleeter'.format(email=user.email),
                password=request.POST.get('current_pwd')
            )

            if test_auth == user:
                user.set_password(new_pwd)

        user.email = email
        user.username = '{email}_fleeter'.format(email=user.email)
        user.display_name = request.POST.get('display_name', '')
        user.phone_number = request.POST.get('phone_number', '')

        user.save()

        request.user = user

        # login(request, user)

        message = 'Updated successfully!'
        message_class = 'alert alert-success'
        message_icon = 'fa fa-check'

        return self.render_to_response(context={
            'page': 'settings',
            'message': message,
            'message_class': message_class,
            'message_icon': message_icon,
        })


class ChoosePlanView(TemplateView):
    template_name = 'users/pricing_table.html'

    def get_context_data(self, **kwargs):
        context = super(ChoosePlanView, self).get_context_data(**kwargs)
        context['page'] = 'settings'
        return context


class AddCustomerView(TemplateView):
    template_name = 'users/add.html'

    def get_context_data(self, **kwargs):
        context = super(AddCustomerView, self).get_context_data(**kwargs)
        context['page'] = 'settings'
        return context


class ActivateUserView(TemplateView):
    template_name = 'users/login.html'

    def dispatch(self, request, *args, **kwargs):
        super(ActivateUserView, self).dispatch(request, *args, **kwargs)
        challenge = request.GET.get('challenge', None)

        if not challenge:
            return self.render_to_response(
                context={
                    'message': 'Identity error',
                    'msg_class': 'danger',
                    'page': 'login'
                })
        else:
            user = Account.objects.filter(challenge=challenge, is_fleeter=True).first()

            if user:
                user.is_active = True
                user.save()
                send_mail_template(
                    'admin@windshieldink.com',
                    user.email,
                    'Welcome to Windshieldink!',
                    'email_template/user/welcome',
                    {
                        'display_name': user.display_name
                    })
                return self.render_to_response(
                    context={
                        'message': 'Your Windshieldink account has been successfully activated!',
                        'msg_class': 'success',
                        'page': 'login'
                    })
            else:
                return self.render_to_response(
                    context={
                        'message': 'Identity error',
                        'msg_class': 'danger',
                        'page': 'login'
                    })
