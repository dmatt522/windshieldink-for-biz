import os
import math


def get_variable_billing_price(plates, tax):
    year_price = (10.0 - (float(0.795) * (float(math.log(plates, math.e)))))
    new_price = year_price / 12.0
    month_price = new_price * plates

    tax_rate = tax.get('rate', 0)
    tax_price = month_price * tax_rate / 100.0

    total_price = month_price + tax_price

    return {
        'per_plate': float("%.2f" % round(new_price, 2)),
        'per_year': float("%.2f" % round(year_price, 2)),
        'per_month': float("%.2f" % round(month_price, 2)),
        'total': float("%.2f" % round(total_price, 2)),
        'tax_rate': float("%0.2f" % tax_rate),
        'tax_type': tax.get('tax'),
        'tax_price': float("%0.2f" % round(tax_price, 2))
    }
