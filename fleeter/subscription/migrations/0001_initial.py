# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import annoying.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='PaymentHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('token', models.CharField(max_length=255, null=True, blank=True)),
                ('checkout_setup_response', annoying.fields.JSONField(null=True, blank=True)),
                ('transaction_details_response', annoying.fields.JSONField(null=True, blank=True)),
                ('execution_response', annoying.fields.JSONField(null=True, blank=True)),
                ('owner', models.ForeignKey(related_name='PaymentHistory_Owner', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
                ('price', models.IntegerField(default=0, blank=True)),
                ('unit_price', models.IntegerField(default=0, blank=True)),
                ('plan_days', models.IntegerField(default=30, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('payment_gateway', models.CharField(max_length=10, null=True, blank=True)),
                ('amount', models.IntegerField(null=True, blank=True)),
                ('start_date', models.DateTimeField(auto_now_add=True)),
                ('expired_at', models.DateTimeField(auto_now_add=True, null=True)),
            ],
        ),
    ]
