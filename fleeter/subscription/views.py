from django.views.generic import TemplateView, View, ListView, DetailView
import requests
import json
import wsi.fleeter_settings as settings
from paypal.interface import PayPalInterface
from paypal.settings import PayPalConfig


class PaymentGateway(TemplateView):
    template_name = 'pages/payments/choose_gateway.html'

    def _pal(self):
        return PayPalInterface(config=PayPalConfig(
            API_USERNAME=settings.PAYPAL_USERID,
            API_PASSWORD=settings.PAYPAL_PASSWORD,
            API_SIGNATURE=settings.PAYPAL_SIGNATURE,
            DEBUG_LEVEL=settings.PAYPAL_TEST
        ))

    def get(self, request, *args, **kwargs):
        pal = self._pal()
        response = pal.set_express_checkout(
            PAYMENTREQUEST_0_AMT=str(30),
            PAYMENTREQUEST_0_PAYMENTACTION='Order',
            PAYMENTREQUEST_0_DESC='test',  # amt=str(price),  #paymentaction='Order',  #desc=description,
            returnurl=settings.PAYPAL_RETURN_URL,
            cancelurl=settings.PAYPAL_CANCEL_URL,
            email='ken-buyer@windshieldink.com'
        )

        assert response is not None, 'Paypal response OKay!'

        return self.render_to_response(context={
            'page': 'index',
            'paykey': ''
        })


class BaseStripeTemplateView(TemplateView):

    def get_context_data(self, **kwargs):
        """"""
        ctx = super(BaseStripeTemplateView, self).get_context_data(**kwargs)
        ctx.update({
            "STRIPE_PUBLIC_KEY": settings.STRIPE_PUBLIC_KEY,
            "plans": settings.PAYMENTS_PLANS,
        })
        return ctx
