from django.conf.urls import url
from fleeter.subscription.views import PaymentGateway
from fleeter.subscription.payment import subscribe_ajax, cancel_subscribe

urlpatterns = [
    url(r'^gateway/$', PaymentGateway.as_view(), name="subscription.gateway"),
    url(r'^checkout/$', subscribe_ajax, name="subscribe_ajax"),
    url(r'^cancel/$', cancel_subscribe, name="cancel_subscribe"),
]
