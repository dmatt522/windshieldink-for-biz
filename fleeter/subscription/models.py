from django.db import models
from apps.users.models import User as Account
from annoying.fields import JSONField


class Plan(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    price = models.IntegerField(default=0, blank=True)
    unit_price = models.FloatField(default=0, blank=True)


class PaymentHistory(models.Model):
    created = models.DateTimeField(auto_now_add=True, blank=True)
    owner = models.ForeignKey(Account, related_name='PaymentHistory_Owner')
    token = models.CharField(max_length=255, blank=True, null=True)
    checkout_setup_response = JSONField(blank=True, null=True)
    transaction_details_response = JSONField(blank=True, null=True)
    execution_response = JSONField(blank=True, null=True)


class Subscription(models.Model):
    payment_gateway = models.CharField(max_length=10, null=True, blank=True)
    plan_id = models.CharField(max_length=30, null=True, blank=True)
    amount = models.IntegerField(null=True, blank=True)
    start_date = models.DateTimeField(auto_now_add=True, blank=True)
    expired_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
