import logging

import wsi.fleeter_settings as settings
from django.shortcuts import redirect

from django.http import HttpResponse, JsonResponse

from django.core.exceptions import ObjectDoesNotExist

from django.views.decorators.http import require_POST

from django.contrib import messages
from django.contrib.auth.decorators import login_required

from fleeter.utils import get_variable_billing_price
from apps.vehicles.models import Vehicles


import stripe

from payments.models import Customer


def wrap_stripe_error(ajax_view):
    """Return a JSONResponse with status 500 if any stripe calls fail for some reason."""

    def wrapper(request):
        try:
            return ajax_view(request)
        except stripe.StripeError as e:
            logging.error('Stripe Error')
            logging.error(e)
            if request.method == "POST":
                logging.error(request.POST)
            return JsonResponse(content=u"%s" % e, status=500)

    return wrapper


@login_required
@wrap_stripe_error
def cancel_subscribe(request):
    request.user.customer.cancel(at_period_end=False)

    vehicles = Vehicles.objects.filter(user=request.user, from_csv=True)
    vehicles.delete()
    user = request.user
    user.active_plates = 0
    user.used_plates = 0
    user.save()

    return redirect('/')


@require_POST
@login_required
@wrap_stripe_error
def subscribe_ajax(request):
    """"""
    stripe_token = request.POST.get("stripe_token")
    plan_id = request.POST.get("plan_id", None)
    price = request.POST.get("price", 0)
    plates = request.POST.get("plates", 0)
    tax_id = request.POST.get("tax_id", 13)
    tax = settings.FLEETER_CANADA_TAXES[13]
    for row in settings.FLEETER_CANADA_TAXES:
        if row.get('id') == int(tax_id):
            tax = row
            break

    prices = get_variable_billing_price(float(plates), tax)
    if float(price) != float(prices.get('total')):
        return JsonResponse({'message': 'Invalid price'})

    try:
        customer = request.user.customer
    except ObjectDoesNotExist:
        customer = Customer.create(request.user)

    stripe.api_key = settings.STRIPE_SECRET_KEY

    if str(plan_id) not in settings.PAYMENTS_PLANS.keys():
        sample_plan = settings.PAYMENTS_PLANS['custom_plan']
        sample_plan['price'] = int(float(price) * 100)
        sample_plan['stripe_plan_id'] = str(plan_id)

        settings.PAYMENTS_PLANS[str(plan_id)] = sample_plan

        try:
            stripe.Plan.create(amount=int(float(price) * 100),
                               interval="month",
                               name=sample_plan['name'],
                               currency="cad",
                               id=str(plan_id))
        except stripe.StripeError as err:
            pass

    customer.update_card(stripe_token)
    customer.subscribe(plan_id)

    user = request.user
    user.active_plates = int(plates)
    user.save()
    request.user = user

    messages.success(request, 'Your subscription is now active.')
    return JsonResponse({'message': 'Subscription successful'})


@require_POST
@login_required
@wrap_stripe_error
def change_plan_ajax(request):
    """"""
    plan_id = request.POST.get("plan_id", None)
    request.user.customer.subscribe(plan_id)
    messages.info(request, 'Subscription plan changed to: %s' % settings.PAYMENTS_PLANS[plan_id]['name'])
    return JsonResponse({'message': 'Subscription changed.'})
