from django import forms

from fleeter.business.models import Company


class CompanyForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={'placeholder': 'Email', 'required': True, 'class': 'form-control'}))

    class Meta:
        model = Company
        fields = ('name', 'hq_phone', 'hq_address', 'formatted_address', 'po_number', 'vendor_number', 'postal_code', 'email',)
