from django.db import models
from wsi.fleeter_settings import SERVER_HOST_NAME


class PlateOrigin(models.Model):
    name = models.CharField(max_length=32, null=True, blank=True)


class CountryProvince(models.Model):
    country = models.CharField(max_length=64, null=True, blank=True)
    province = models.CharField(max_length=64, null=True, blank=True)

    def get_provices(self):
        provinces = CountryProvince.objects.filter(country=self.country).all()
        ret_obj = list()

        for province in provinces:
            ret_obj.append({
                'province': province.province
            })

        return ret_obj


class Company(models.Model):
    name = models.CharField(max_length=32, null=True, blank=True)
    email = models.EmailField(blank=True, null=True)
    hq_phone = models.CharField(max_length=32, null=True, blank=True)
    hq_address = models.CharField(max_length=255, null=True, blank=True)
    formatted_address = models.CharField(max_length=1024, null=True, blank=True)
    po_number = models.CharField(max_length=32, null=True, blank=True)
    vendor_number = models.CharField(max_length=32, null=True, blank=True)
    postal_code = models.CharField(max_length=32, null=True, blank=True)
    sites = models.CharField(max_length=16, null=True, blank=True)
    logo = models.ImageField(upload_to='logos/', null=True, blank=True)
    biz_id = models.CharField(max_length=100, null=True, blank=True)

    def logo_url(self):
        """
        Returns the URL of the image associated with this Object.
        If an image hasn't been uploaded yet, it returns a stock image

        :returns: str -- the image url

        """
        if self.logo and hasattr(self.logo, 'url'):
            return 'http://{server}{url}'.format(server=SERVER_HOST_NAME, url=self.logo.url)
        else:
            return 'http://{server}/media/logos/user-1.png'.format(server=SERVER_HOST_NAME)
