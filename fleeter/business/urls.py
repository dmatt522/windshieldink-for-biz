from django.conf.urls import url

from fleeter.business.views import DashboardView, PlatesView, CompanyView, \
    PlatesEditView, CompanyBillingView, PlatesUploadView, FormulaView, ProvinceView, UploadLogoView, delete_message, \
    delete_plate, PlateAddView, PlatesDownloadView


urlpatterns = [
    url(r'^dashboard/$', DashboardView.as_view(), name="biz.dashboard"),
    url(r'^delete_message/(?P<pk>[0-9]+)/$', delete_message, name="biz.delete_message"),
    url(r'^plates/$', PlatesView.as_view(), name="biz.plates"),
    url(r'^plates/(?P<pk>[0-9]+)/$', PlatesEditView.as_view(), name="biz.plates.edit"),
    url(r'^plates/delete/(?P<pk>[0-9]+)/$', delete_plate, name="biz.plates.delete"),
    url(r'^plates/upload/$', PlatesUploadView.as_view(), name="biz.plates.upload"),
    url(r'^plates/download/$', PlatesDownloadView.as_view(), name="biz.plates.download"),
    url(r'^company/$', CompanyView.as_view(), name="biz.company"),
    url(r'^company/logo/$', UploadLogoView.as_view(), name="biz.company.logo"),
    url(r'^province/$', ProvinceView.as_view(), name="biz.province"),
    url(r'^billing/$', CompanyBillingView.as_view(), name="biz.company.billing"),
    url(r'^formula/$', FormulaView.as_view(), name="biz.formula"),
    url(r'^plate/new/$', PlateAddView.as_view(), name="biz.plate.add"),
]
