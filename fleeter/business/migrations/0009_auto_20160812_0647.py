# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0008_company_biz_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='country',
        ),
        migrations.RemoveField(
            model_name='company',
            name='province',
        ),
        migrations.AddField(
            model_name='company',
            name='formatted_address',
            field=models.CharField(max_length=1024, null=True, blank=True),
        ),
    ]
