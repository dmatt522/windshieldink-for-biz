# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from wsi.fleeter_settings import BASE_DIR
import csv
import os


def load_origins(apps, schema_editor):
    PlateOrigin = apps.get_model('business', 'PlateOrigin')
    csv_path = os.path.join(BASE_DIR, 'fixtures/origin.csv')
    print '{csv_path}'.format(csv_path=csv_path)
    with open(csv_path) as f:
        for row in csv.reader(f):
            origin = PlateOrigin(name=row[0])
            print "Insert origin {origin}".format(origin=row[0])
            origin.save()

    print "Loaded Plate Origin Successfully!"


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_origins)
    ]
