# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0005_auto_20160804_2116'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='country',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
