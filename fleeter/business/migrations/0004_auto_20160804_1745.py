# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from wsi.settings import BASE_DIR
import os
import csv


def load_countries(apps, schema_editor):
    CountryProvince = apps.get_model('business', 'CountryProvince')
    csv_path = os.path.join(BASE_DIR, 'fixtures/countries.csv')
    print '{csv_path}'.format(csv_path=csv_path)
    with open(csv_path) as f:
        for row in csv.reader(f):
            origin = CountryProvince(country=row[0], province=row[1])
            origin.save()

    print "Loaded CountryProvince Successfully!"


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0003_countryprovince'),
    ]

    operations = [
        migrations.RunPython(load_countries)
    ]
