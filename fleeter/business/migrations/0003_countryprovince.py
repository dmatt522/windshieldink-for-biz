# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0002_auto_20160804_0357'),
    ]

    operations = [
        migrations.CreateModel(
            name='CountryProvince',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country', models.CharField(max_length=64, null=True, blank=True)),
                ('province', models.CharField(max_length=64, null=True, blank=True)),
            ],
        ),
    ]
