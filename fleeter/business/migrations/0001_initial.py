# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32, null=True, blank=True)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('hq_phone', models.CharField(max_length=32, null=True, blank=True)),
                ('hq_address', models.CharField(max_length=255, null=True, blank=True)),
                ('province', models.CharField(max_length=255, null=True, blank=True)),
                ('po_number', models.CharField(max_length=32, null=True, blank=True)),
                ('vendor_number', models.CharField(max_length=32, null=True, blank=True)),
                ('postal_code', models.CharField(max_length=32, null=True, blank=True)),
                ('sites', models.CharField(max_length=16, null=True, blank=True)),
                ('logo', models.CharField(max_length=32, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='PlateOrigin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32, null=True, blank=True)),
            ],
        ),
    ]
