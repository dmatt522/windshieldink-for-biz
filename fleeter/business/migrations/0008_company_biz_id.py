# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0007_remove_company_owner'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='biz_id',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
