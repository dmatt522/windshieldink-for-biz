from django.shortcuts import render, redirect
from django.views.generic import TemplateView, View, FormView
from fleeter.account.views import LoginRequiredMixin
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
import csv
from apps.vehicles.models import Vehicles
from apps.message.models import Messages
from fleeter.business.models import PlateOrigin, Company, CountryProvince
from fleeter.business.forms import CompanyForm
from fleeter.account.forms import ImageUploadForm

from fleeter.utils import get_variable_billing_price
from wsi.fleeter_settings import STRIPE_PUBLIC_KEY, FLEETER_CANADA_TAXES

from django.core.exceptions import ObjectDoesNotExist
from payments.models import Customer

from django.db.models import Q


class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context['page'] = 'index'
        return context

    def dispatch(self, request, *args, **kwargs):
        super(DashboardView, self).dispatch(request, *args, **kwargs)

        page = request.GET.get('page', 0)
        query = request.GET.get('q', None)
        if query == '' or query is None:
            query = ''

        vehicles = Vehicles.objects.filter(user=request.user).all()
        vids = list()
        for vehicle in vehicles:
            vids.append(vehicle.id)

        if query != '':
            messages = Messages.objects.filter(vid__in=vids, text__contains=query).all()
        else:
            messages = Messages.objects.filter(vid__in=vids).all()

        message_vehicles = list()
        for message in messages:
            vehicle = Vehicles.objects.filter(pk=message.vid).first()
            if vehicle not in message_vehicles:
                message_vehicles.append(vehicle)

        return self.render_to_response(context={
            'vehicles': message_vehicles,
            'messages': messages,
            'total': messages.count(),
            'page': 'index',
            'query': query
        })


class PlatesView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/plates/index.html'

    def get_context_data(self, **kwargs):
        context = super(PlatesView, self).get_context_data(**kwargs)
        context['page'] = 'plates'
        return context

    def get(self, request, *args, **kwargs):
        order_by = request.GET.get('orderby', 'plate')
        plates = Vehicles.objects.filter(user=request.user, is_fleeter=True).order_by(order_by)
        query = request.GET.get('q', '')

        if query != '':
            plates = Vehicles.objects\
                .filter(user=request.user, is_fleeter=True)\
                .filter(Q(plate__contains=query.upper()) | Q(secondary_email__contains=query) | Q(formatted_address__contains=query))\
                .order_by(order_by)

        return self.render_to_response(context={
            'plates': plates,
            'page': 'plates',
            'query': query
        })


class PlatesEditView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/plates/edit.html'

    def get_context_data(self, **kwargs):
        context = super(PlatesEditView, self).get_context_data(**kwargs)
        context['page'] = 'plates'
        return context

    def get(self, request, pk, *args, **kwargs):
        plate = Vehicles.objects.filter(pk=pk).first()
        origins = PlateOrigin.objects.all()
        return self.render_to_response(context={
            'page': 'plates',
            'plate': plate,
            'origins': origins,
        })

    def post(self, request, pk, *args, **kwargs):
        plate = Vehicles.objects.filter(pk=pk).first()

        plate.formatted_address = request.POST.get('origin', '')
        plate.secondary_email = request.POST.get('email', '')
        plate.plate = request.POST.get('plate', '')

        plate.save()

        origins = PlateOrigin.objects.all()

        return self.render_to_response(context={
            'page': 'plates',
            'plate': plate,
            'origins': origins,
            'message': 'Updated successfully!',
            'message_class': 'alert alert-success',
        })


class PlatesUploadView(TemplateView):
    template_name = 'pages/plates/upload.html'

    def get_context_data(self, **kwargs):
        context = super(PlatesUploadView, self).get_context_data(**kwargs)
        context['page'] = 'plates'
        context['message'] = ''
        return context

    def dispatch(self, request, *args, **kwargs):
        return super(PlatesUploadView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        csvfile = request.FILES['csv_file']

        added = 0
        failed = 0
        duplicated = 0
        headerpass = False

        active_plates = request.user.active_plates
        used_plates = request.user.used_plates

        try:
            if request.user.customer.current_subscription.status != 'active':
                return self.render_to_response(context={
                    'message': 'You don\'t have paid active subscription',
                    'page': 'plates'
                })
            elif active_plates == 0 or active_plates - used_plates == 0:
                return self.render_to_response(context={
                    'message': 'You have used all the slots. Please change your plan.',
                    'page': 'plates'
                })
        except:
            return self.render_to_response(context={
                'message': 'You don\'t have paid active subscription',
                'page': 'plates'
            })

        for row in csv.reader(csvfile):
            if headerpass is False:
                headerpass = True
            else:
                plate_origin = PlateOrigin.objects.filter(name=row[1]).first()
                if used_plates < active_plates:
                    if plate_origin is not None and row[2] is not None and row[2] != '':
                        exist = Vehicles.objects.filter(plate=row[0].upper()).first()
                    if exist is None and row[2] != '':
                        plate = Vehicles(user=request.user, from_csv=True)
                        try:
                            plate.formatted_address = plate_origin.name
                        except:
                            plate.formatted_address = 'Unknown'
                        plate.plate = row[0].upper()
                        plate.secondary_email = row[2]
                        plate.is_fleeter = True
                        plate.save()
                        used_plates += 1
                        added += 1
                    else:
                        if row[2] != '' and exist is not None:
                            try:
                                exist.formatted_address = plate_origin.name
                            except:
                                exist.formatted_address = 'Unknown'
                            exist.plate = row[0].upper()
                            exist.secondary_email = row[2]
                            exist.is_fleeter = True
                            exist.from_csv = True
                            exist.save()
                            duplicated += 1
                else:
                    failed += 1
        user = request.user
        user.used_plates = used_plates
        user.save()
        request.user = user

        return self.render_to_response(context={
            'message': '{added} row(s) added, {duplicated} row(s) duplicated, {failed} row(s) invalid.'.format(
                added=added, duplicated=duplicated, failed=failed
            )
        })


class PlatesDownloadView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="export.csv"'

        writer = csv.writer(response)
        writer.writerow(['Plate Number', 'Plate Origin', 'Contact Email'])
        plates = Vehicles.objects.filter(is_fleeter=True, from_csv=True, user=request.user).all()

        for plate in plates:
            writer.writerow([plate.plate, plate.formatted_address, plate.secondary_email])

        return response


class ProvinceView(View):
    def get(self, request):
        country = request.GET.get('country', '')
        ret_obj = list()
        if country != '':
            provinces = CountryProvince.objects.filter(country=country).all()
            for province in provinces:
                ret_obj.append(province.province)

        return JsonResponse({'data': ret_obj})


class CompanyView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/company/index.html'

    def get_context_data(self, **kwargs):
        context = super(CompanyView, self).get_context_data(**kwargs)
        context['page'] = 'company'
        return context

    @staticmethod
    def get_countries():
        countries = CountryProvince.objects.all()
        ret_obj = list()
        for country in countries:
            if country.country not in ret_obj:
                ret_obj.append(country.country)

        return ret_obj

    def get(self, request, *args, **kwargs):
        user = request.user

        return self.render_to_response(context={
            'page': 'company',
            'company': request.user.company,
            'countries': self.get_countries(),
        })

    def post(self, request, *args, **kwargs):
        form = CompanyForm(data=request.POST)
        old_company = request.user.company
        if form.is_valid():
            company = form.save(commit=False)
            company.logo = old_company.logo
            company.biz_id = old_company.biz_id
            company.owner = request.user
            company.id = old_company.id
            company.save()

        request.user.company = company

        return self.render_to_response(context={
            'page': 'company',
            'company': company,
            'message': 'Updated successfully!',
            'countries': self.get_countries(),
        })


class CompanyBillingView(TemplateView):
    template_name = 'pages/payments/billing.html'

    def get(self, request, *args, **kwargs):
        try:
            customer = request.user.customer
        except ObjectDoesNotExist:
            customer = Customer.create(request.user)
            request.user.customer = customer
            return self.render_to_response(context={
                'STRIPE_PUBLIC_KEY': STRIPE_PUBLIC_KEY,
                'page': 'company',
                'taxes': FLEETER_CANADA_TAXES
            })
        try:
            if request.user.customer.current_subscription.status == 'active':
                return self.render_to_response(context={
                    'message': 'You already have an active paid subscription.',
                    'STRIPE_PUBLIC_KEY': STRIPE_PUBLIC_KEY,
                    'page': 'company',
                    'taxes': FLEETER_CANADA_TAXES
                })
            else:
                return self.render_to_response(context={
                    'STRIPE_PUBLIC_KEY': STRIPE_PUBLIC_KEY,
                    'page': 'company',
                    'taxes': FLEETER_CANADA_TAXES
                })
        except:
            return self.render_to_response(context={
                'STRIPE_PUBLIC_KEY': STRIPE_PUBLIC_KEY,
                'page': 'company',
                'taxes': FLEETER_CANADA_TAXES
            })


class FormulaView(View):
    def post(self, request, *args, **kwargs):
        plates = request.POST.get('plates', 0)
        tax_id = request.POST.get('tax_id', 1)

        tax = FLEETER_CANADA_TAXES[13]
        for row in FLEETER_CANADA_TAXES:
            if row.get('id') == int(tax_id):
                tax = row
                break

        price = get_variable_billing_price(float(plates), tax)

        return JsonResponse(price)


class UploadLogoView(FormView):
    form_class = ImageUploadForm

    def post(self, request, *args, **kwargs):
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            m = request.user.company
            m.logo = form.cleaned_data['image']
            m.save()
            request.user.company = m

        return redirect('/biz/company/')


class PlateAddView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/plates/add.html'

    def get_context_data(self, **kwargs):
        context = super(PlatesEditView, self).get_context_data(**kwargs)
        context['page'] = 'plates'
        return context

    def get(self, request, *args, **kwargs):
        origins = PlateOrigin.objects.all()
        return self.render_to_response(context={
            'page': 'plates',
            'origins': origins,
        })

    def post(self, request, *args, **kwargs):
        plate_str = request.POST.get('plate', '')
        # check if the same plate exists
        vehicle = Vehicles.objects.filter(plate=plate_str.upper(), formatted_address=request.POST.get('origin', '')).first()
        if vehicle is not None:
            origins = PlateOrigin.objects.all()
            return self.render_to_response(
                context={
                    'page': 'plates',
                    'origins': origins,
                    'message': 'This plate is already exist on the database',
                    'msgclass': 'alert alert-danger'
                }
            )
        plate = Vehicles(
            formatted_address=request.POST.get('origin', ''),
            secondary_email=request.POST.get('email', ''),
            plate=plate_str.upper(),
            from_csv=True,
            is_fleeter=True,
            user=request.user
        )

        plate.save()

        request.user = refresh_user_plates(request.user)

        return redirect('/biz/plates/')


def refresh_user_plates(user):
    plates = Vehicles.objects.filter(user=user, from_csv=True).all()
    user.used_plates = plates.count()
    user.save()
    return user


@login_required
def delete_message(request, pk):
    message = Messages.objects.filter(pk=int(pk)).first()
    if message is not None:
        message.delete()

    return redirect('/biz/dashboard/')


@login_required
def delete_plate(request, pk):
    plate = Vehicles.objects.filter(user=request.user, pk=pk).first()

    if plate is not None:
        plate.delete()
        request.user = refresh_user_plates(request.user)

    return redirect('/biz/plates/')
